#include <Servo.h>

#define RUDDER_PIN 2
#define SPEED_PIN 5
#define MODE_PIN_1 4
#define MODE_PIN_2 3

#define RIGHT 1850
#define LEFT 850

Servo rudder;

int angle = (RIGHT + LEFT) / 2;
int old_angle = angle;
int wheels = 0;
int old_wheels = 0;

void setup() {
  rudder.attach(RUDDER_PIN);
  rudder.writeMicroseconds(angle);

  analogWrite(SPEED_PIN, wheels);

  pinMode(MODE_PIN_1, OUTPUT);
  pinMode(MODE_PIN_2, OUTPUT);
  forward();

  Serial.begin(115200);
}

String command = "";
char ch;

void ParseCommand(void) {
  String ang = "";
  String whe = "";
  int angle_tmp = 0;
  bool next = false;
  char ch;
  for (int i = 0; i < command.length(); i++) {
    ch = command.charAt(i);
    if (ch == ',') {
      next = true;
    } else {
      if (next) {
        whe += ch;
      } else {
        ang += ch;
      }
    }
  }
  angle_tmp = ang.toInt()
  angle = LEFT + (angle_tmp + 30) * ((RIGHT - LEFT) / 60);
  wheels = whe.toInt();
  if (angle > RIGHT) angle = RIGHT;
  if (angle < LEFT) angle = LEFT;
  if (wheels > 255) wheels = 255;
  if (wheels < -255) wheels = -255;
}

void ReadCommand(void) {
  while (Serial.available()) {
    ch = Serial.read();
    if (ch == '\n') {
      ParseCommand();
      command = "";
      break;
    } else {
      command += ch;
    }
  }
}

void forward(void) {
  digitalWrite(MODE_PIN_1, HIGH);
  digitalWrite(MODE_PIN_2, LOW);
}

void backward(void) {
  digitalWrite(MODE_PIN_1, LOW);
  digitalWrite(MODE_PIN_2, HIGH);
}

void loop() {
  if (old_angle != angle) {
    rudder.writeMicrosecond(angle);
    old_angle = angle;
  }
  if (old_wheels != wheels) {
    if (wheels > 0) {
      forward();
      analogWrite(SPEED_PIN, wheels);
    } else {
      backward();
      analogWrite(SPEED_PIN, wheels * -1);
    }
    old_wheels = wheels;
  }
  ReadCommand();
  delay(1);
}
